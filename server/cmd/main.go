package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	grpcdatastream "gitlab.com/v.s.krivoshein/ikitlab/proto"
	"gitlab.com/v.s.krivoshein/ikitlab/server/internal/repository"
	"gitlab.com/v.s.krivoshein/ikitlab/server/internal/service"
	"gitlab.com/v.s.krivoshein/ikitlab/server/internal/transport/grpcapi"
	"os/signal"
	"strconv"
	"syscall"

	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	logrus.Info("Starting app")

	rdb := repository.NewRedisDB(getRedisConfig())
	repo := repository.New(rdb)
	srv := service.New(repo, getServiceConfig())
	server := grpcapi.New(srv)

	grpcPort := fmt.Sprintf(":%s", os.Getenv("GRPC_PORT"))
	listen, err := net.Listen("tcp", grpcPort)
	if err != nil {
		logrus.Fatalf("net listen fatal: %v", err)
	}

	s := grpc.NewServer()
	grpcdatastream.RegisterDataStreamServer(s, server)

	go func() {
		if err = s.Serve(listen); err != nil {
			logrus.Errorf("fatal serve grpc server err: %v", err)
		}
	}()

	// Listen for the interrupt signal.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	logrus.Info("received os.signal: " + (<-quit).String())

	s.Stop()

	if err = rdb.Close(); err != nil {
		logrus.Fatal("fail db connection close " + err.Error())
	}

	logrus.Info("server exiting")
}

func getServiceConfig() *service.Config {
	viper.AddConfigPath(".")
	viper.SetConfigName("config")
	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("fatal read config: %v", err)
	}
	return &service.Config{
		URLs:             viper.GetStringSlice("URLs"),
		MinTimeout:       viper.GetInt("MinTimeout"),
		MaxTimeout:       viper.GetInt("MaxTimeout"),
		NumberOfRequests: viper.GetInt("NumberOfRequests"),
		MaxConnections:   toInt(os.Getenv("MAX_CONNECTIONS")),
	}
}

func getRedisConfig() *repository.RedisConfig {
	return &repository.RedisConfig{
		Addr: fmt.Sprintf("%v:%v", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
	}
}

func toInt(v string) int {
	i, err := strconv.Atoi(v)
	if err != nil {
		logrus.Fatal("fail convert env to bool")
	}
	return i
}
