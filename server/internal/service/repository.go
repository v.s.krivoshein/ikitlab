package service

import (
	"context"
	"time"
)

type Repository interface {
	Get(ctx context.Context, url string) (*string, error)
	Set(ctx context.Context, url string, bodyString *string, ttl time.Duration) error
}
