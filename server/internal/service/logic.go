package service

import (
	"context"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/singleflight"
	"io"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

type Config struct {
	URLs             []string
	MinTimeout       int
	MaxTimeout       int
	NumberOfRequests int
	MaxConnections   int
}

type service struct {
	repo    Repository
	config  *Config
	client  *http.Client
	sfGroup singleflight.Group
	sem     chan struct{}
}

func New(repo Repository, config *Config) Cache {
	rand.Seed(time.Now().Unix())
	return &service{
		repo:    repo,
		config:  config,
		client:  newHTTPClient(config.MaxConnections),
		sfGroup: singleflight.Group{},
		sem:     make(chan struct{}, config.MaxConnections),
	}
}

func (s *service) GetRandomDataStream(ctx context.Context) chan *string {
	ch := make(chan *string)
	var wg sync.WaitGroup
	wg.Add(s.config.NumberOfRequests)
	// запускаем параллельно получение ответов по url
	for i := 0; i < s.config.NumberOfRequests; i++ {
		go func() {
			defer wg.Done()
			randomURL := s.config.URLs[rand.Intn(len(s.config.URLs))] // nolint
			// Предотвращаем параллельное исполнение одинаковых вычислений используя singleflight
			URLRespBodyInterface, err, _ := s.sfGroup.Do(randomURL, func() (interface{}, error) {
				// Ключевая логика в getURLResponse. Вынес в отдельную функцию, для удобочитаемости.
				return s.getURLResponse(ctx, randomURL)
			})
			if err != nil {
				logrus.Errorf("err s.sfGroup.Do: %v ", err)
				return
			}
			// Приводим к нужному типу и пишем в канал
			URLRespBody, ok := URLRespBodyInterface.(*string)
			if !ok {
				return
			}
			ch <- URLRespBody
		}()
	}

	// Дожидаемся завершения всех запросов и закрываем канал
	go func(wg *sync.WaitGroup, ch chan *string) {
		wg.Wait()
		close(ch)
	}(&wg, ch)

	return ch
}

var failRequest = "fail request"
var noTTL time.Duration

func (s *service) getURLResponse(ctx context.Context, randomURL string) (interface{}, error) {
	// Внимание, проверка на отсутствие ошибки! (Для избежания большой вложенности)
	// Сразу получили результат из кэша и возвращаем его
	URLResponse, err := s.repo.Get(ctx, randomURL)
	if err == nil {
		return URLResponse, nil
	}

	// Если в кэше не оказалось результата выполняем запрос и сохраняем в кэш
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, randomURL, http.NoBody)
	if err != nil {
		return nil, err
	}
	// Ограничиваем максимальное количество параллельно выполняемых запросов
	s.sem <- struct{}{}
	res, err := s.client.Do(req)
	if err != nil {
		s.handleErr(randomURL)
		return nil, err
	}
	defer func() { <-s.sem }()
	defer res.Body.Close()

	bodyBytes, err := io.ReadAll(res.Body)
	if err != nil {
		s.handleErr(randomURL)
		return nil, err
	}

	bodyString := string(bodyBytes)
	ttl := time.Duration(rand.Intn(s.config.MaxTimeout-s.config.MinTimeout)+s.config.MinTimeout) * time.Second // nolint
	err = s.repo.Set(ctx, randomURL, &bodyString, ttl)
	if err != nil {
		s.handleErr(randomURL)
		return nil, err
	}

	return &bodyString, nil
}

func (s *service) handleErr(randomURL string) {
	// Сообщаем слушателям, что не удалось получить результат
	err := s.repo.Set(context.Background(), randomURL, &failRequest, noTTL)
	if err != nil {
		logrus.Errorf("fail inform redis listener that request is fail, err: %v", err)
	}
}

func newHTTPClient(maxConnectionsPerHost int) *http.Client {
	t := http.DefaultTransport.(*http.Transport).Clone()
	t.MaxIdleConns = -1
	t.MaxConnsPerHost = maxConnectionsPerHost
	t.DisableKeepAlives = true

	return &http.Client{
		Timeout:   5 * time.Second,
		Transport: t,
	}
}
