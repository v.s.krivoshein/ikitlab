package service

import "context"

type Cache interface {
	GetRandomDataStream(ctx context.Context) chan *string
}
