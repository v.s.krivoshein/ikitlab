package repository

import (
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
)

type RedisConfig struct {
	Addr     string
	Password string
	DB       int
}

func NewRedisDB(c *RedisConfig) *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     c.Addr,
		Password: c.Password,
		DB:       c.DB,
	})
	_, err := client.Ping().Result()
	if err != nil {
		logrus.Fatal("redis connection ping failed" + err.Error())
	}

	return client
}
