package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"gitlab.com/v.s.krivoshein/ikitlab/server/internal/service"
	"time"
)

type repository struct {
	client *redis.Client
}

func New(client *redis.Client) service.Repository {
	return &repository{
		client: client,
	}
}

const requiredGetRequest = "get request and publish result"
const requiredSubscribe = "another process is getting value, subscribe and await result"

func (r *repository) Get(ctx context.Context, url string) (*string, error) {
	// Lua script для redis:
	// 1) В случае если нет данных по url, то автоматически устанавливает флаг requiredSubscribe
	// (другой процесс начал получать значение по url и после опубликует его для всех подписчиков.)
	// Вернет requiredGetRequest
	// 2) В случае если уже установлен флаг requiredSubscribe по URL запроса, вернет данный флаг
	// 3) Если получен есть кэш bodyString, то вернет bodyString
	//
	// Если реализовать отдельными запросами, то существует вероятность, что между запросами другой сервис также успеет
	// запросить данные по URL и не найдя результат начнет параллельно выполнять запрос к url
	query := fmt.Sprintf(`
		local bodyString = redis.call('get', ARGV[1]);
		local notFound = '%v';
		local pending = '%v';
		if bodyString == false then
			redis.call('set', ARGV[1], pending);
			return notFound;
		end ;
		return bodyString;`,
		requiredGetRequest,
		requiredSubscribe,
	)
	var err error
	result, err := r.client.WithContext(ctx).Eval(query, []string{"0"}, url).Result()
	if err != nil {
		return nil, err
	}

	// Необходимо сделать запрос сервису и сохранить в кэш
	if result == requiredGetRequest {
		return nil, errors.New("cache is not exist")
	}

	// Другой процесс делает запрос, подписываемся на канал и ждем результата
	if result == requiredSubscribe {
		subscriber := r.client.Subscribe(url)
		msg, rsvErr := subscriber.ReceiveMessage() // rsvErr for preventing shadow declaration
		if err != nil {
			return nil, rsvErr
		}
		return &msg.Payload, nil
	}

	// Получен готовый кэш, который возвращаем
	bodyString, ok := result.(string)
	if !ok {
		logrus.Errorf("unexpected type of cash, must be string, receive %T", bodyString)
		return nil, errors.New("unexpected type of cash")
	}

	return &bodyString, err
}

func (r *repository) Set(ctx context.Context, url string, bodyString *string, ttl time.Duration) error {
	_, err := r.client.WithContext(ctx).TxPipelined(func(pipe redis.Pipeliner) error {
		// записываем в кэш
		err := pipe.Set(url, *bodyString, ttl).Err()
		if err != nil {
			logrus.Errorf("err set redis cash %v", err)
			return err
		}
		// публикуем для подписчиков
		err = pipe.Publish(url, *bodyString).Err()
		if err != nil {
			logrus.Errorf("err publish redis cash %v", err)
			return err
		}
		_, err = pipe.Exec()
		if err != nil {
			logrus.Errorf("fail exec pipe redis err: %v", err)
			return err
		}
		return nil
	})

	if err != nil {
		logrus.Errorf("unexpected err rdb: %v", err)
		return err
	}

	return nil
}
