package grpcapi

import (
	"github.com/sirupsen/logrus"
	grpc "gitlab.com/v.s.krivoshein/ikitlab/proto"
	"gitlab.com/v.s.krivoshein/ikitlab/server/internal/service"
	"google.golang.org/protobuf/types/known/emptypb"
)

type GRPCServer struct {
	service service.Cache
	grpc.UnimplementedDataStreamServer
}

func New(s service.Cache) grpc.DataStreamServer {
	return &GRPCServer{service: s}
}

func (s *GRPCServer) GetRandomDataStream(_ *emptypb.Empty, stream grpc.DataStream_GetRandomDataStreamServer) error {
	dataStream := s.service.GetRandomDataStream(stream.Context())
	for data := range dataStream {
		err := stream.Send(&grpc.GetRandomDataStreamRes{
			Data: *data,
		})
		if err != nil {
			logrus.Errorf("fail stream send err: %v", err)
			return err
		}
	}
	return nil
}
