protos:
	protoc \
	-I=/proto \
	--go-grpc_out=/proto \
	--go_out=/proto \
	/*.proto

up:
	docker-compose up --build

down:
	docker-compose down