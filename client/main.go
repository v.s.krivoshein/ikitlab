package main

import (
	"context"
	"github.com/sirupsen/logrus"
	grpcdatastream "gitlab.com/v.s.krivoshein/ikitlab/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/protobuf/types/known/emptypb"
	"io"
	"sync"
)

func main() {
	logrus.SetFormatter(new(logrus.JSONFormatter))
	logrus.Info("start client")
	conn, err := grpc.Dial("server:8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		logrus.Fatalf("fail grpc dial err: %v", err)
	}

	c := grpcdatastream.NewDataStreamClient(conn)

	cycles := 1000
	var wg sync.WaitGroup
	wg.Add(cycles)
	for i := 0; i < cycles; i++ {
		go func() {
			defer wg.Done()
			stream, err := c.GetRandomDataStream(context.Background(), &emptypb.Empty{})
			if err != nil {
				logrus.Fatalf("fail call GetRandomDataStream err: %v", err)
			}

			for {
				_, err = stream.Recv()
				if err == io.EOF {
					break
				}
				if err != nil {
					logrus.Fatalf("fail stream get random data stream err: %v", err)
				}
			}
		}()
	}

	wg.Wait()

	logrus.Infof("success, received all parts")
}
